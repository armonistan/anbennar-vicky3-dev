﻿STATE_YYL_MOITSA = {
    id = 633
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00C558" "x00E7D8" "x02A3A1" "x02AB38" "x06F5C3" "x0D9113" "x0FB42B" "x117ADC" "x13E6ED" "x165B27" "x1A3604" "x1A6574" "x1A9940" "x1B7927" "x1BAFD3" "x1DF15A" "x1E1496" "x205808" "x208DC1" "x21C6E9" "x24B558" "x2730B3" "x289EB5" "x29E77A" "x2F0071" "x2F5BAD" "x317F29" "x33C3A3" "x342006" "x347AB8" "x38541A" "x3C4CF5" "x4354BF" "x43CED9" "x45C21D" "x4A88BD" "x4FEB7A" "x519E48" "x53686B" "x55185E" "x55640E" "x55CE19" "x58D052" "x59658F" "x5D9D25" "x62E171" "x62E76C" "x631575" "x668AA7" "x66F802" "x676EAC" "x6A6DE7" "x6B22DD" "x6C0FB7" "x6DD6EC" "x70F4E9" "x715381" "x7766A1" "x78E734" "x808096" "x82DC3D" "x8324CC" "x871CE6" "x8A2AF1" "x8BDE09" "x8E35A5" "x8E7AC5" "x8FACC9" "x90E54C" "x9401B5" "x945243" "x97BECF" "x984E7F" "x9A5858" "x9AF43F" "x9F6CD6" "x9FF4F6" "xA0D28E" "xA29827" "xA3A805" "xA3E003" "xA49864" "xA53E01" "xA8CD78" "xAA1682" "xAC362E" "xAD1C34" "xAD2E08" "xAFD665" "xB1EBE7" "xB2C56F" "xB2F3EF" "xB79E15" "xB8CDA0" "xB98D09" "xBA10C7" "xBD7E3B" "xBDE65F" "xC0107C" "xC3BD1C" "xC55D89" "xC731FB" "xC79DC0" "xC8B156" "xCEAD94" "xCF1C94" "xCFF534" "xD1C50C" "xD2806E" "xD351B8" "xD50988" "xD56F78" "xD63049" "xD8DD4B" "xD9FCA0" "xDC32A2" "xDD0B6D" "xDE9B76" "xE11374" "xE2767F" "xE592E1" "xE80AAC" "xE8B915" "xE99378" "xEA6E14" "xEE1913" "xF41829" "xFEF78B" "xFF7A91" "xFFD569" }
    impassable = { "xD1C50C" "xA3E003" }
    traits = { state_trait_valak_river }
    city = "x205808"
    farm = "x205808"
    mine = "x205808"
    port = "x205808"
    wood = "x205808"
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 13
        bg_coal_mining = 36
        bg_fishing = 9
        bg_whaling = 8
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
    naval_exit_id = 3308 #Hokhos Sea
}
STATE_MOITSA = {
    id = 634
    subsistence_building = "building_subsistence_farms"
    provinces = { "x020E48" "x0A398E" "x0B6271" "x0CE9BA" "x12D786" "x1A6B6C" "x230D9E" "x25969B" "x26CC62" "x304EB5" "x432D92" "x4F20E5" "x5540EB" "x5555DA" "x56D9FD" "x5C598D" "x66AA9A" "x67D248" "x730C84" "x7AB06F" "x7B895F" "x7E3682" "x7E4519" "x7F33ED" "x81F595" "x830968" "x8853DA" "x8B860F" "x8FC6DD" "x93DE6A" "x9D0B06" "xA1BBC8" "xA949DA" "xAEC0BB" "xB6BFFD" "xB9B5C4" "xBE9540" "xC01DAB" "xC2F281" "xCEDA9D" "xD16B18" "xD24AD7" "xD2A5E3" "xD8256D" "xDB42C8" "xDC4B4E" "xE0E6A6" "xEC0F1F" "xEF56A5" "xF1AA8E" "xF8E907" "xFCD009" "xFD91C9" "xFF1B28" }
    traits = { state_trait_valak_river }
    city = "x25969B"
    farm = "x25969B"
    mine = "x25969B"
    wood = "x25969B"
    arable_land = 50
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 23
        bg_coal_mining = 36
        bg_sulfur_mining = 8
    }
}
STATE_MOITSADHI = {
    id = 635
    subsistence_building = "building_subsistence_farms"
    provinces = { "x015B08" "x0744B9" "x0AF7F2" "x104960" "x1110F1" "x1803BD" "x1F92A9" "x3000B6" "x33B8A5" "x37BCBB" "x3809EC" "x3AD288" "x3BE790" "x3C54DA" "x406FFE" "x4141CC" "x451B88" "x4E5397" "x50077E" "x50A97C" "x55411B" "x5B1524" "x5F05EE" "x5FF25D" "x6550B4" "x6DFFD7" "x73DF8D" "x7B1648" "x7DD14D" "x80832F" "x8D8793" "x91101A" "xA58DA3" "xAF80E0" "xB8EE59" "xC027DD" "xC353DC" "xC5F813" "xC9CE99" "xD49614" "xDD6507" "xDD9E4E" "xE6610F" "xEA4715" "xEE1D3D" "xEEAB6C" }
    traits = { state_trait_valak_river }
    city = "x0AF7F2"
    farm = "x0AF7F2"
    mine = "x0AF7F2"
    port = "x0AF7F2"
    wood = "x0AF7F2"
    arable_land = 70
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 19
        bg_coal_mining = 36
        bg_fishing = 8
    }
    naval_exit_id = 3309 #Blue Sea
}
STATE_SKURKHA_KYARD = {
    id = 636
    subsistence_building = "building_subsistence_farms"
    provinces = { "x11520C" "x172885" "x36BF62" "x3CA04A" "x6477CC" "x788351" "x7D2D8B" "xB421FA" "xCA2609" "xCA2E42" "xF53F22" "xFBB3DC" }
    traits = { state_trait_valak_river }
    city = "x11520C"
    farm = "x11520C"
    mine = "xF53F22"
    wood = "xFBB3DC"
    arable_land = 57
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 15
        bg_iron_mining = 21
        bg_coal_mining = 28
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 15
    }
}
STATE_OVTO_KIGVAL = {
    id = 637
    subsistence_building = "building_subsistence_farms"
    provinces = { "x076C61" "x0F26CC" "x29E4D4" "x32C6D8" "x3FA67F" "x79DEF2" "x819F8D" "x89B7AD" "x8CBA03" "x8DAE50" "xA2795A" "xDCC8DE" "xF83446" }
    traits = { state_trait_valak_river }
    city = "xA2795A"
    farm = "xF83446"
    mine = "x29E4D4"
    wood = "x79DEF2"
    arable_land = 62
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 11
        bg_lead_mining = 27
        bg_iron_mining = 15
        bg_coal_mining = 36
    }
}
STATE_KVAKEINOLBA = {
    id = 638
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0CA7A2" "x117D0F" "x168796" "x1BED69" "x4AE0FB" "x50C897" "x6A6BE1" "x78112C" "x892C2F" "xA022E2" "xA17E22" "xB5F43B" "xD3A673" "xF17173" "xF45685" "xF4C792" }
    traits = { state_trait_valak_river }
    city = "x0CA7A2"
    farm = "x1BED69"
    mine = "xB5F43B"
    wood = "x50C897"
    arable_land = 68
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 7
        bg_iron_mining = 96
        bg_coal_mining = 48
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 10
        discovered_amount = 2
    }
}
STATE_DZIMOKLI = {
    id = 640
    subsistence_building = "building_subsistence_farms"
    provinces = { "x032813" "x055FD1" "x13294B" "x223F23" "x273CEC" "x305121" "x310528" "x32219E" "x386A9A" "x390DD8" "x3B295A" "x44528E" "x4877DD" "x6D2CC3" "x6ED756" "x754C44" "x9EB9C6" "xC36904" "xCA957B" "xDAC45D" "xE262BB" "xE69B1A" "xEDA7AA" "xF777B1" "xFDCA00" }
    traits = { state_trait_valak_river state_trait_dzimokli_coal_fields }
    city = "x9EB9C6"
    farm = "x273CEC"
    mine = "x6ED756"
    wood = "x386A9A"
    arable_land = 85
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 5
        bg_lead_mining = 18
        bg_iron_mining = 33
        bg_coal_mining = 72
    }
}
STATE_FOIRAKHIAN = {
    id = 642
    subsistence_building = "building_subsistence_farms"
    provinces = { "x010DFE" "x03C97F" "x08B891" "x0BDB8B" "x0D78D0" "x0EEF6F" "x1082EC" "x1914D2" "x20DB67" "x272896" "x33E591" "x35C15F" "x3A74DA" "x47F53F" "x4A32A9" "x4A63E7" "x4C56F0" "x5832C7" "x5DC2D3" "x635E60" "x63CD17" "x683AEE" "x7746CF" "x7CFE7C" "x8137E4" "x82466D" "x845A35" "x90F3A1" "x96C35E" "x9D0AA1" "x9E4874" "xA2073C" "xA5F823" "xAB8821" "xBE6A18" "xBFF26A" "xC63842" "xC76C36" "xD0AE0E" "xD0D23C" "xD476E8" "xD52578" "xD57D54" "xD96841" "xEEC8E9" "xF9EB9D" }
    traits = {}
    city = "x010DFE"
    farm = "x8137E4"
    mine = "xD0D23C"
    wood = "x9D0AA1"
    arable_land = 30
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
        bg_sulfur_mining = 8
    }
}
STATE_SHEVROMRZGH = {
    id = 643
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10C218" "x1EE4FE" "x28D434" "x2CB4B3" "x2E4F7C" "x2EF89A" "x334778" "x349681" "x363D22" "x3BE960" "x3D4A83" "x3DEB9F" "x57B06C" "x6003E7" "x61C4DD" "x674F67" "x6B6C70" "x6F75ED" "x71CE87" "x725048" "x8C97C4" "x992DD9" "xA3E644" "xAFB765" "xB916C2" "xD070B6" "xD1D554" "xD9622E" "xE24A71" "xFE4FDF" }
    traits = {}
    city = "x6B6C70"
    farm = "x8C97C4"
    mine = "x2E4F7C"
    wood = "x992DD9"
    arable_land = 62
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 7
        bg_lead_mining = 12
    }
}
STATE_NAGLAIBAR = {
    id = 644
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0629BB" "x0CF492" "x1829CE" "x192B8F" "x1D954E" "x275C5D" "x4B23FB" "x546150" "x5B526F" "x5B66C1" "x5D0985" "x6002E2" "x6744F0" "x67E7AA" "x7001F8" "x7C8A50" "x81D34C" "x81EBF5" "x8BB887" "x9C10CA" "xA02FC5" "xA64DE0" "xA6D587" "xAA0F29" "xACEE60" "xB0997E" "xB926BA" "xBBAB57" "xBDBC28" "xC65214" "xC90C9B" "xCF080C" "xD33772" "xD7BC11" "xE99453" "xEC56AD" "xEEE151" "xF58385" "xF7F653" }
    traits = { state_trait_forbidden_plains }
    city = "xA6D587"
    farm = "xA64DE0"
    mine = "xBBAB57"
    wood = "x8BB887"
    arable_land = 65
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 7
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 25
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
}
STATE_UGHEABAR = {
    id = 645
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A1FF9" "x1367D7" "x1AADF8" "x22103F" "x360420" "x36CAFC" "x39CACA" "x3DEC00" "x43F7AB" "x45A8BD" "x48EE75" "x4D61C5" "x5701DA" "x77DF2D" "x787759" "x7EBFD2" "x8AB034" "x8FF5DF" "xA7B36A" "xC97635" "xCCDFC4" "xF76975" "xFDA67A" "xFF09DF" "xFFEF4A" }
    traits = { state_trait_forbidden_plains }
    city = "xFF09DF"
    farm = "xCCDFC4"
    mine = "x1367D7"
    wood = "x43F7AB"
    arable_land = 40
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
    }
}
STATE_CAUBHEAMEAS = {
    id = 647
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x022BAA" "x04B3DD" "x0586B4" "x0829C2" "x11C4D7" "x16A7F0" "x175CB2" "x1A7F9A" "x1AFCE1" "x222C01" "x2731F8" "x273C2B" "x2EBE9D" "x304606" "x322B2E" "x34837A" "x36A5CA" "x37B844" "x385521" "x3CB6C7" "x4354AA" "x4523D3" "x462492" "x468AFB" "x486D56" "x48981F" "x494CA4" "x4A78D3" "x4D883E" "x4F530C" "x502083" "x528FAF" "x546700" "x5A6E4B" "x5A8447" "x5B1F81" "x5CAC42" "x5DF8E6" "x603C1E" "x60B07D" "x60F231" "x611AB1" "x640A55" "x670190" "x69DE3D" "x6AB122" "x6D3847" "x6F1969" "x708D39" "x738A3A" "x73A235" "x75BAC4" "x7C2C22" "x7D522C" "x7D96CE" "x817B57" "x81ED35" "x846B7C" "x86DBD9" "x8791C4" "x8A8224" "x8CD7EE" "x91F7E1" "x964B52" "x9879F8" "x998359" "x99CD62" "x9BD57D" "x9CB25C" "x9D0EB2" "x9FB977" "xA2237C" "xA67017" "xABF971" "xAC0012" "xAE51B6" "xB18A82" "xB18DAD" "xB69024" "xB7182B" "xB88C88" "xBA01B4" "xBED380" "xBF4B1F" "xC0441A" "xC3BCEA" "xCA0739" "xCB8ED2" "xCC4245" "xCCEEA3" "xCD83BF" "xCFBE56" "xD159C3" "xD42832" "xDA4CD8" "xDA626D" "xDB204A" "xDB204B" "xDB204C" "xDB204D" "xE39EC5" "xE5576A" "xE737CA" "xEF69BD" "xF07D2D" "xF46121" "xF53072" "xF64BD8" "xF7D731" "xFBA5C4" "xFCFCE8" "xFD2AEE" "xFDC464" }
    traits = {}
    city = "x462492"
    farm = "x462492"
    mine = "x462492"
    wood = "x462492"
    arable_land = 25
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 24
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 25
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}
STATE_SERPENT_GIFT = {
    id = 646
    subsistence_building = "building_subsistence_farms"
    provinces = { "x008FBE" "x01E59A" "x08B43A" "x0AEC52" "x0DF765" "x156E2B" "x2186CA" "x22D751" "x23AC25" "x294C95" "x34ADFB" "x3B2C66" "x45147A" "x4BA8F2" "x59C3EF" "x60587B" "x624555" "x6281A2" "x6D2BE9" "x70FD0C" "x77DE09" "x77FF70" "x7B00EA" "x8378EE" "x8C0E08" "x9004B7" "x9097A6" "x936170" "x961628" "x9AEEAF" "xADD126" "xB048E5" "xB0993B" "xB50E7A" "xB66138" "xB89C1A" "xBA526C" "xBD1779" "xC3A743" "xCA93F6" "xDDD41E" "xE47AA3" "xE69F1A" "xE6AED5" "xED0E36" "xEE7E65" "xF18A0F" "xF3CC24" "xF4B2FF" "xF6FF57" "xF858DD" }
    traits = { state_trait_egoirlust_river }
    city = "x01E59A"
    farm = "x01E59A"
    mine = "x01E59A"
    wood = "x01E59A"
    arable_land = 101
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_opium_plantations bg_silk_plantations bg_tobacco_plantations bg_cotton_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
        bg_iron_mining = 21
        bg_coal_mining = 60
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
}
STATE_SARLUN_GOILUST = {
    #
    id = 650
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05B74C" "x0E4E0B" "x111FE6" "x114030" "x36358C" "x3BE014" "x53FA6E" "x5C9007" "x61A3E3" "x63A92C" "x6AC3EC" "x792C1D" "x7DAEF4" "x808011" "x808090" "x9088C7" "x97975E" "x9F22DB" "xA5584A" "xB56DB4" "xBA7909" "xC00000" "xC04D30" "xD00C27" "xEBABAC" "xEC439A" "xF423DB" }
    traits = { state_trait_egoirlust_river state_trait_forbidden_plains }
    city = "x36358C"
    farm = "x36358C"
    mine = "x36358C"
    wood = "x36358C"
    arable_land = 44
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 6
        bg_iron_mining = 21
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}
STATE_IRDU_AGEENEAS = {
    id = 648
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00CE95" "x01701B" "x027351" "x06BE02" "x079FDD" "x0A8194" "x0CDD46" "x1B1536" "x24687F" "x27B9D7" "x284ABF" "x2A26E9" "x31E989" "x3282DD" "x333C7C" "x355E3C" "x37CC33" "x3E0BA4" "x3EE58E" "x407F94" "x4330D6" "x489820" "x4AA713" "x4CFD74" "x4D4E5E" "x4DF8C6" "x4E7962" "x57A455" "x58500A" "x589A9F" "x589B54" "x5CDEE1" "x618B1B" "x638266" "x645A15" "x64670D" "x65DA09" "x66C888" "x68837C" "x6C6C08" "x724487" "x7435F0" "x791ED2" "x7A6730" "x82A48D" "x82EB6C" "x83B441" "x8B48A0" "x8E8CBF" "x92FD98" "x945826" "x94D92E" "x95D458" "x9B5266" "xA23699" "xA25851" "xA68C77" "xAAFCB5" "xC010E3" "xC1C9D5" "xC3D26E" "xC66CE3" "xCA11DC" "xCA588D" "xCE12D1" "xD1953C" "xD43CE7" "xD489E8" "xD4E687" "xD571A7" "xD72A8A" "xDB204F" "xDB2050" "xDB2051" "xDB2052" "xDB2053" "xDB2054" "xE167BB" "xEBA0EE" "xEDBC86" "xEFF4AC" "xF4ACAF" "xFF2915" }
    traits = {}
    city = "x489820"
    farm = "x489820"
    mine = "x489820"
    wood = "x489820"
    arable_land = 43
    arable_resources = { bg_livestock_ranches bg_opium_plantations bg_silk_plantations bg_tobacco_plantations bg_cotton_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_sulfur_mining = 20
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 40
    }
}
STATE_GHANEERSP = {
    id = 649
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x02C752" "x039355" "x048D72" "x05E599" "x06A6DD" "x0A4DD6" "x0D7FB7" "x0E25AA" "x12B689" "x167D3D" "x171C80" "x175159" "x17C6FE" "x197417" "x1E84F7" "x202974" "x2E518C" "x316FAD" "x32EFF0" "x36697E" "x3A5DA8" "x3C254D" "x3CAC14" "x3EC592" "x42CEF9" "x4A6D9C" "x4ABAEB" "x4EC226" "x516E4A" "x5FC71A" "x601E26" "x6313D9" "x644D88" "x66E7D9" "x70F041" "x77798D" "x7C69FA" "x7CF0B7" "x7D5BFB" "x7EA361" "x81F991" "x832955" "x843645" "x867CDD" "x884026" "x89CEF0" "x8C202A" "x8F374D" "x905FD6" "x9B1B79" "x9B2324" "xA35C5E" "xA4A34E" "xA6ABE0" "xA924E4" "xABE0D4" "xB26E38" "xB2ABDA" "xB66513" "xB8A2D5" "xBF9E15" "xBFBD93" "xC4D0BD" "xC678BF" "xC81EA2" "xC83FCE" "xCA9F19" "xD0B5BE" "xD4498E" "xD47DD6" "xD98825" "xDB0046" "xDB825D" "xDDE627" "xE4B72B" "xE59F4F" "xF202CD" "xF2C5DE" "xF5B1D4" "xFB9ED4" "xFC63BC" "xFDC3D5" }
    traits = { state_trait_forbidden_plains }
    city = "x1E84F7"
    farm = "x1E84F7"
    mine = "x1E84F7"
    wood = "x1E84F7"
    arable_land = 84
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_lead_mining = 18
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 25
    }
}
STATE_SOCHULEAG = {
    id = 651
    subsistence_building = "building_subsistence_farms"
    provinces = { "x052AD3" "x08E2AE" "x0B8953" "x176BE8" "x1A012A" "x1A9D71" "x1B88EF" "x1E841E" "x3030D2" "x3D5A5D" "x4C3948" "x4DCE0E" "x505F64" "x6DAC1E" "x74A6A9" "x803344" "x80C401" "x848E35" "x893839" "x97B34B" "x9C7C3C" "x9D1D81" "x9EA06C" "xA44617" "xA4EEB5" "xAE80DC" "xB3155A" "xC23BB9" "xC5EFAA" "xC8C92C" "xE14B3B" "xEC338D" "xF73546" "xF8E956" }
    traits = { state_trait_valak_river state_trait_forbidden_plains }
    city = "xEC338D"
    farm = "xC8C92C"
    mine = "x74A6A9"
    wood = "x4C3948"
    arable_land = 83
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 20
        bg_fishing = 11
        bg_coal_mining = 28
    }
    naval_exit_id = 3310 #Kodarve Lake
}
STATE_ADOI_FILEANAN = {
    id = 652
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E7119" "x115976" "x141C93" "x179BB0" "x2170F4" "x2FD41A" "x303390" "x4B05DC" "x5207B2" "x64011C" "x6CA418" "x7D908D" "x862E03" "x9ECF24" "x9F9682" "xA0A154" "xA540CB" "xB83A2F" "xC045CE" "xC90EA4" "xCC35E4" "xCCD89A" "xCDAC5A" "xD6110A" "xD693AB" "xDE1BB6" "xE26770" "xE63DF2" "xF6C660" "xFC7FD8" "xFC93AB" }
    traits = { state_trait_egoirlust_river state_trait_forbidden_plains }
    city = "xD693AB"
    farm = "x179BB0"
    mine = "xE63DF2"
    port = "xD693AB"
    wood = "xA540CB"
    arable_land = 136
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 7
        bg_fishing = 4
        bg_sulfur_mining = 40
    }
    naval_exit_id = 3310 #Kodarve Lake
}
STATE_ORCHEKH = {
    id = 653
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A17D9" "x0FB84C" "x16ADA2" "x21D238" "x225917" "x22A709" "x4169BF" "x48D6CE" "x4BBCDF" "x535535" "x536123" "x5B6491" "x5F7B45" "x63CEB5" "x6A69AA" "x7091EC" "x7C4DD7" "x7C4E81" "x7EEBE6" "x80E168" "x82AEEA" "xA0BAD0" "xA674B5" "xABB52D" "xAF3598" "xB8AC65" "xBB312B" "xC58F87" "xCC29B4" "xD0A0D0" "xE0D9AF" "xE4ADFB" "xE5B909" "xEB1C69" "xEEF3B9" "xF21007" "xFD17FD" }
    traits = { state_trait_forbidden_plains }
    city = "x4169BF"
    farm = "x535535"
    mine = "x80E168"
    port = "xB8AC65"
    wood = "x82AEEA"
    arable_land = 95
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 10
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_APORLAEN = {
    id = 654
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B39E0" "x1122E1" "x157463" "x2B625B" "x30B20D" "x37F671" "x3AEC95" "x3B0176" "x49E501" "x4F1BC4" "x506E5E" "x5BC534" "x5BCE47" "x627FF9" "x82AAAD" "x85CB5B" "x90E34A" "x96C163" "xA03BDF" "xA56292" "xA58559" "xA72C70" "xAB42CE" "xADAFB1" "xB4C583" "xB51C81" "xC0CE2A" "xCC9C60" "xCCBB53" "xE15846" "xE94E09" "xF2F77F" "xF835BF" }
    traits = { state_trait_natural_harbors state_trait_kozyoshdek_river state_trait_forbidden_plains }
    city = "x96C163"
    farm = "xA56292"
    mine = "xA58559"
    port = "x37F671"
    wood = "x3B0176"
    arable_land = 82
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 4
        bg_iron_mining = 24
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_ZARMIKLON = {
    id = 655
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AE686" "x1B76C4" "x1BC4A6" "x2511A2" "x261D9B" "x2863F6" "x297963" "x2C4366" "x3127E5" "x3874D5" "x4D47BB" "x5D6F1E" "x5DBF9E" "x7A4A07" "x8B6169" "x904030" "xBD6CF3" "xC04012" "xC6E2E7" "xCDE174" "xE0D3F9" "xF4CEF3" "xF61012" "xFBB47F" }
    traits = { state_trait_forbidden_plains }
    city = "x5D6F1E"
    farm = "x2863F6"
    mine = "x3874D5"
    port = "xFBB47F"
    wood = "x8B6169"
    arable_land = 109
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 6
        bg_iron_mining = 18
        bg_coal_mining = 12
        bg_sulfur_mining = 40
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_BULREKAYIG = {
    id = 656
    subsistence_building = "building_subsistence_farms"
    provinces = { "x068859" "x0A4B4E" "x0AA399" "x11C030" "x2FC68B" "x3C498D" "x3EB730" "x4696AD" "x4A71B7" "x4AAA58" "x687ABB" "x692DF6" "x702A8C" "x749129" "x76F040" "x77ABFA" "x79CEFC" "x880696" "x8C4823" "x983611" "x9AFA6A" "xA85F09" "xB905E5" "xBA0D99" "xBE5CDB" "xBE99E0" "xC21B54" "xC312BF" "xC47E1B" "xC60700" "xC67774" "xC7FD89" "xCB5B7E" "xD72502" "xE85D89" "xE86E1F" "xE956E8" }
    traits = { state_trait_volutsabaj_river state_trait_forbidden_plains }
    city = "xA85F09"
    farm = "x702A8C"
    mine = "x4A71B7"
    port = "xD72502"
    wood = "x77ABFA"
    arable_land = 131
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 7
        bg_iron_mining = 40
        bg_coal_mining = 12
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    naval_exit_id = 3313 #Zenruuk Lake
}
STATE_AKANDHIL = {
    id = 657
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03F9E1" "x2CE750" "x3D22B3" "x485920" "x4AE046" "x517EF4" "x5CAC91" "x68E6E2" "x75D184" "x7E74AB" "x7F6449" "x815FD1" "x86A5AA" "x9F438F" "xA87FE4" "xAFEC31" "xB6943C" "xBC29F3" "xBEA265" "xC8483F" "xCA6547" "xCFB449" "xEA051E" "xF1C98D" "xF4CF14" "xF85CF5" }
    traits = { state_trait_akandhil_mineral_fields state_trait_mudholkin_river }
    city = "xF85CF5"
    farm = "xC8483F"
    mine = "x517EF4"
    wood = "x7F6449"
    arable_land = 23
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
        bg_iron_mining = 21
        bg_coal_mining = 28
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 7
        discovered_amount = 1
    }
}
STATE_TOZGONREK = {
    id = 658
    subsistence_building = "building_subsistence_farms"
    provinces = { "x086D7B" "x0E0134" "x20201C" "x251EB0" "x2BD326" "x37807C" "x3ADE63" "x4122F8" "x48E840" "x562717" "x5E8F10" "x6B877D" "x8659F7" "x88669F" "x933C2B" "xB9104E" "xC25D9D" "xC41B96" "xCBA1FA" "xE54467" "xE69A65" "xEBB583" "xFDFB3F" }
    traits = { state_trait_natural_harbors state_trait_mudholkin_river }
    city = "xCBA1FA"
    farm = "x933C2B"
    mine = "x48E840"
    port = "xE69A65"
    wood = "x0E0134"
    arable_land = 84
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 13
        bg_iron_mining = 45
        bg_coal_mining = 32
    }
    naval_exit_id = 3309 #Blue Sea
}
STATE_ZOITAL = {
    id = 659
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0646CE" "x08E83A" "x0A9415" "x0FDAD6" "x1000F0" "x1080F0" "x137B97" "x160E3F" "x2C3A6E" "x3BCC0A" "x3CB2BD" "x47DFC6" "x48B486" "x48FE5F" "x59DF28" "x6249B5" "x808DE6" "x810EEB" "x8E1A24" "x9000F0" "x9080F0" "x91D53E" "xA943B7" "xB0BC94" "xCCD9EE" "xD0B89C" "xE02E1B" "xE8704B" }
    traits = {}
    city = "x8E1A24"
    farm = "xE8704B"
    mine = "x6249B5"
    wood = "x0646CE"
    arable_land = 39
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 23
        bg_lead_mining = 12
        bg_iron_mining = 75
        bg_coal_mining = 36
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 8
    }
}
STATE_ZUURZOI = {
    id = 660
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AA01A" "x2909FD" "x298F9B" "x29C0BA" "x2C2C24" "x2D6E8E" "x4D3C57" "x4DE223" "x53F284" "x5FE13B" "x61C91B" "x7DECD0" "x8749DD" "x8CEFDA" "x9FCCD9" "xAFF43A" "xB2E71F" "xB48ED2" "xB6642B" "xC4FF1C" "xC8EC59" "xE37CF0" "xE59D9C" "xEF0E17" "xF3D71B" "xF72864" "xFBC4F4" }
    traits = {}
    city = "x2909FD"
    farm = "x7DECD0"
    mine = "x298F9B"
    port = "x53F284"
    wood = "xEF0E17"
    arable_land = 58
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 9
        bg_whaling = 7
        bg_logging = 17
        bg_lead_mining = 21
        bg_iron_mining = 37
        bg_coal_mining = 32
        bg_sulfur_mining = 32
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
	resource = {
        type = "bg_relics_digsite"
        undiscovered_amount = 5
    }
    naval_exit_id = 3308 #Hokhos Sea
}
STATE_PRIKOYOL = {
    #canal place TODO
    id = 661
    subsistence_building = "building_subsistence_farms"
    provinces = { "x37895A" "x494EB5" "x4C0F9E" "x5533AC" "x614242" "x623DF1" "xE76B56" "xFF586D" }
    traits = { state_trait_kalyins_gift }
    city = "xE76B56"
    farm = "x37895A"
    mine = "x623DF1"
    port = "x494EB5"
    wood = "x614242"
    arable_land = 212
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 15
        bg_logging = 7
        bg_lead_mining = 10
        bg_sulfur_mining = 30
    }
    naval_exit_id = 3310 #Kodarve Lake
}
STATE_GADHLUMO = {
    id = 662
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0EB0AE" "x306177" "x561832" "x5A61DF" "x6A26BD" "x8B4AC3" "xABD505" "xCC6BEA" "xD8C27E" "xE2CB30" "xFEFF38" }
    traits = { state_trait_kalyins_gift }
    city = "xFEFF38"
    farm = "xABD505"
    mine = "x5A61DF"
    port = "x6A26BD"
    wood = "xE2CB30"
    arable_land = 251
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_dye_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 14
        bg_logging = 12
        bg_iron_mining = 15
        bg_sulfur_mining = 35
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3309 #Blue Sea
}
STATE_PEENADHI = {
    id = 663
    subsistence_building = "building_subsistence_farms"
    provinces = { "x025279" "x2C3458" "x377F84" "x435DCE" "x925CA5" "xD4AB21" "xDED078" "xE6763F" }
    traits = { state_trait_kalyins_gift state_trait_natural_harbors }
    city = "x377F84"
    farm = "x2C3458"
    mine = "x435DCE"
    port = "x925CA5"
    wood = "xD4AB21"
    arable_land = 172
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 8
        bg_iron_mining = 22
        bg_sulfur_mining = 32
    }
    naval_exit_id = 3310 #Kodarve Lake
}
STATE_YUKARON = {
    id = 664
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1EADAB" "x21E8F0" "x39E063" "x49F7B7" "x4C9B67" "x55B68D" "x6BE172" "x733DDF" "x9AD48A" "x9F41D8" "xC06F20" "xF930B0" }
    traits = { state_trait_kalyins_gift }
    city = "x49F7B7"
    farm = "x4C9B67"
    mine = "x55B68D"
    port = "xF930B0"
    wood = "x9F41D8"
    arable_land = 247
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 13
        bg_lead_mining = 32
        bg_sulfur_mining = 22
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_YUKAROYOL = {
    id = 665
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A322C" "x283AF1" "x65DE80" "x6F5B60" "x98C01A" "xC6833A" "xD73827" "xDD811B" "xFBC49D" }
    traits = { state_trait_kalyins_gift }
    city = "xC6833A"
    farm = "x65DE80"
    mine = "xDD811B"
    port = "xC6833A"
    wood = "x98C01A"
    arable_land = 189
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 25
        bg_lead_mining = 30
        bg_sulfur_mining = 10
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_QUSHYIL = {
    id = 666
    subsistence_building = "building_subsistence_farms"
    provinces = { "x139F9F" "x7C881B" "x86A979" "x9B1750" "xA268F8" "xB4AB17" "xDD7863" }
    traits = { state_trait_kalyins_gift }
    city = "x7C881B"
    farm = "x9B1750"
    mine = "xB4AB17"
    port = "x7C881B"
    wood = "xDD7863"
    arable_land = 143
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 8
        bg_iron_mining = 10
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 16
    }
    naval_exit_id = 3311 #Yukelqur Lake
}
STATE_KESH_GOLKHIN = {
    id = 667
    subsistence_building = "building_subsistence_farms"
    provinces = { "x266F9C" "x29755D" "x58D24E" "x5E9CD0" "x976C2E" "xA1D52F" "xA1F1AA" "xBA64F0" "xC8C1D7" "xD4DB0B" }
    traits = { state_trait_kalyins_gift }
    city = "xD4DB0B"
    farm = "x58D24E"
    mine = "x266F9C"
    port = "x976C2E"
    wood = "x5E9CD0"
    arable_land = 369
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 12
        bg_logging = 10
        bg_iron_mining = 25
    }
    naval_exit_id = 3313 #Zenruuk Lake
}
STATE_SARTZ_NEISAR = {
    #btw this needs to be a canal place TODO
    id = 639
    subsistence_building = "building_subsistence_farms"
    provinces = { "x522332" "x677497" "x7DCD58" "x865426" "xDDB6AC" "xF3EB35" "xFA2B20" }
    traits = { state_trait_kalyins_gift }
    city = "x7DCD58"
    farm = "x522332"
    mine = "x865426"
    port = "x7DCD58"
    wood = "xF3EB35"
    arable_land = 177
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 11
        bg_whaling = 3
        bg_fishing = 13
        bg_lead_mining = 24
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3309 #Blue Sea
}
STATE_SHIKENKHIIN = {
    id = 641
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3439FE" "x35599F" "x6E690E" "x726217" "x893833" "xC414E5" }
    traits = { state_trait_kalyins_gift state_trait_ultakal_mines }
    city = "xC414E5"
    farm = "x893833"
    mine = "x726217"
    port = "x3439FE"
    wood = "x6E690E"
    arable_land = 255
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 8
        bg_whaling = 5
        bg_fishing = 7
        bg_coal_mining = 25
        bg_gold_mining = 4
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    naval_exit_id = 3313 #Zenruuk Lake
}
STATE_ZOI_KHORKHIIN = {
    id = 668
    subsistence_building = "building_subsistence_farms"
    provinces = { "x102244" "x1D5F99" "x5712AF" "x651B87" "x9D7AF7" "xED4750" "xF351AD" }
    traits = { state_trait_kalyins_gift }
    city = "x9D7AF7"
    farm = "x651B87"
    mine = "x5712AF"
    port = "x9D7AF7"
    wood = "xF351AD"
    arable_land = 115
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 17
        bg_whaling = 7
        bg_fishing = 12
        bg_lead_mining = 22
    }
    naval_exit_id = 3309 #Blue Sea
}
STATE_SHIK_DAZAR = {
    id = 669
    subsistence_building = "building_subsistence_farms"
    provinces = { "x059E60" "x301E6D" "x39B4EC" "x3BDFA7" "x799B74" "xAA0916" "xB46A85" "xD6383C" "xED28D0" "xF4F267" }
    traits = { state_trait_kalyins_gift }
    city = "xF4F267"
    farm = "xB46A85"
    mine = "x3BDFA7"
    port = "xF4F267"
    wood = "xAA0916"
    arable_land = 180
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 12
        bg_whaling = 8
        bg_fishing = 13
        bg_lead_mining = 45
    }
	resource = {
        type = "bg_relics_digsite"
        undiscovered_amount = 5
    }
    naval_exit_id = 3309 #Blue Sea
}
