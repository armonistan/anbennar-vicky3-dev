﻿#Holds
pmg_base_building_dwarven_hold = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive
	production_methods = {
		pm_hold_simple
		pm_hold_developing
		pm_hold_established
		pm_hold_advanced
	}
}
pmg_transportation_building_dwarven_hold = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	ai_selection = most_productive
	production_methods = {
		pm_hold_passages
		pm_hold_pulleys
		pm_hold_dagrite_ele
		pm_hold_electric_ele
	}
}
pmg_specialization_building_dwarven_hold = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_hold_no_spec
		pm_hold_farm
		pm_hold_foundry
		pm_hold_artisan
		pm_hold_military
	}
}
pmg_automation_building_dwarven_hold = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_hold_no_auto
		pm_hold_vents
		pm_hold_spells
		pm_hold_doors
		pm_hold_control
	}
}

#Dwarovrod
pmg_base_building_dwarovrod = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_dwarovrod_minecarts
		pm_dwarovrod_restored_trains
		pm_dwarovrod_runic_engines
		pm_dwarovrod_artifice_engines
	}
}
pmg_rails_building_dwarovrod = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_dwarovrod_basic_restoration
		pm_dwarovrod_dagrite
		pm_dwarovrod_mithril
	}
}
pmg_automation_building_dwarovrod = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_dwarovrod_no_auto
		pm_dwarovrod_golems_basic
		pm_dwarovrod_golems_assistants
		pm_dwarovrod_golems_conductors
	}
}


#Serpentbloom Farms
pmg_base_building_serpentbloom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_farming
		pm_soil_enriching_farming
		pm_fertilization
		pm_chemical_fertilizer
	}
}
pmg_secondary_building_serpentbloom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_serpentbloom_no_secondary
		pm_serpentbloom_opium
	}
}
pmg_harvesting_process_building_serpentbloom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_tools_disabled
		pm_tools
		pm_steam_threshers
		pm_tractors
		pm_compression_ignition_tractors
		pm_automata_harvesters
		pm_autonomous_tractors
	}
}



#Mushroom Farms
pmg_base_building_mushroom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_farming
		pm_soil_enriching_farming
		pm_fertilization
		pm_chemical_fertilizer
	}
}
pmg_secondary_building_mushroom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_mushroom_no_secondary
		pm_mushroom_liquor
	}
}
pmg_harvesting_process_building_mushroom_farm = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_tools_disabled
		pm_tools
		pm_steam_threshers
		pm_tractors
		pm_compression_ignition_tractors
		pm_automata_harvesters
		pm_autonomous_tractors
	}
}

#Cave Coral
pmg_base_building_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_forestry_cave_coral
		pm_saw_mills_cave_coral
		pm_summoned_blades_cave_coral
		pm_electric_saw_mills_cave_coral
		pm_old_growth_regeneration_sawmills_cave_coral
	}
}
pmg_hardwood_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_hardwood_cave_coral
		pm_hardwood_cave_coral
		pm_increased_hardwood_cave_coral
		pm_hardwood_transmutation_cave_coral
	}
}
pmg_equipment_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_equipment
		pm_steam_donkey_building_logging_camp
		pm_chainsaws
		pm_shredder_mechs_cave_coral
		pm_automata_loggers_cave_coral
	}
}
pmg_transportation_building_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_building_logging_camp
		pm_log_carts
	}
}

#Mithril Mine
pmg_mining_equipment_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_mithril_mine
		pm_atmospheric_engine_pump_building_mithril_mine
		pm_condensing_engine_pump_building_mithril_mine
		pm_diesel_pump_building_mithril_mine
	}
}
pmg_explosives_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_explosives
		pm_nitroglycerin_building_mithril_mine
		pm_dynamite_building_mithril_mine
	}
}
pmg_steam_automation_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_steam_automation
		pm_steam_donkey_mine
	}
}
pmg_train_automation_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}

#Gem Mine
pmg_mining_equipment_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_gem_mine
		pm_atmospheric_engine_pump_building_gem_mine
		pm_condensing_engine_pump_building_gem_mine
		pm_diesel_pump_building_gem_mine
		pm_transmutative_heavy_pump_gem_mine # Anbennar
	}
}

pmg_explosives_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_explosives
		pm_nitroglycerin_building_gem_mine
		pm_dynamite_building_gem_mine
		pm_drill_mining_servos_gem_mine # Anbennar
		pm_subterrenes_building_gem_mine # Anbennar
	}
}

pmg_steam_automation_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_steam_automation
		pm_steam_donkey_mine
		pm_automata_miners # Anbennar
		pm_automata_foremen # Anbennar
	}
}
pmg_train_automation_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}