﻿A82 = { #Escann
	use_culture_states = yes
	is_major_formation = yes
	unification_play = dp_unify_escann
	leadership_play = dp_leadership_escann

	required_states_fraction = 0.85

	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = nationalism
	}
}

Y02 = { #Yanshen
	use_culture_states = yes
	
	is_major_formation = yes
	
	unification_play = dp_unify_yanshen
	leadership_play = dp_leadership_yanshen

	required_states_fraction = 0.6
	
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			OR = {
				country_has_primary_culture = cu:jiangszun
				country_has_primary_culture = cu:beikling
				country_has_primary_culture = cu:gangim
				country_has_primary_culture = cu:naamjyut
				country_has_primary_culture = cu:jiangyang
			}
			has_technology_researched = nationalism
		}
	}
}

A01 = { #Anbennar
	use_culture_states = yes
	
	is_major_formation = yes
	
	unification_play = dp_unify_anbennar
	leadership_play = dp_leadership_anbennar

	required_states_fraction = 0.6
	
	ai_will_do = { always = yes }

	possible = {
	}
}

#Serpentreach
D62 = { 
	is_major_formation = yes
	unification_play = dp_unify_serpentreach
	leadership_play = dp_leadership_serpentreach

	states = {
		STATE_ARG_ORDSTUN
		STATE_ORLGHELOVAR
        STATE_SHAZSTUNDIHR
        STATE_VERKAL_SKOMDIHR
		STATE_OVDAL_LODHUM
        STATE_GOR_BURAD
		STATE_ARGROD_TERMINUS
        STATE_ARGROD_JUNCTION
        STATE_ARGROD
		STATE_DIAMOND_QUARRY
    }
	required_states_fraction = 1

	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = nationalism
		always = yes
	}
}

#Ruincliff (North Aelantiri) Federation
B67 = {
	use_culture_states = yes

	is_major_formation = yes

	unification_play = dp_unify_ruincliffs
	leadership_play = dp_leadership_ruincliffs

	required_states_fraction = 0.7
	
	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = pan-nationalism
		#has_law = law_type:law_cultural_exclusion
		NOT = { is_country_type = unrecognized }
	}
}