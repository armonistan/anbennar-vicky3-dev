﻿COUNTRIES = {
	c:C54 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = tradition_of_equality
		
		effect_starting_politics_traditional = yes

		#Laws
		

		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_nation_of_artifice
		activate_law = law_type:law_no_migration_controls
	}
}