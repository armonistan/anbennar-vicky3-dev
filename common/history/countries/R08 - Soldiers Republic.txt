﻿COUNTRIES = {
	c:R08 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		activate_law = law_type:law_stratocracy
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_dedicated_police #Militarized police not possible without mass surveillance
		activate_law = law_type:law_no_schools #Religious schools not possible with serfdom
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned

		set_tax_level = very_high

		add_modifier = { name = hobgoblin_statocracy_modifier }
		add_journal_entry = { type = je_command_splinter }
	}
}