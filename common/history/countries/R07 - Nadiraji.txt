﻿COUNTRIES = {
	c:R07 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = mandatory_service
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy #Until stratocracy is created
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_dedicated_police #Militarized police not possible without mass surveillance
		activate_law = law_type:law_no_schools #Religious schools not possible with serfdom
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice
	}
}