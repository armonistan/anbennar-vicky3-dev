COUNTRIES = {
	c:A27 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_cultural_exclusion	#most tolerant of Escann
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_expanded_tolerance	#most tolerant of Escann
		
		
		activate_law = law_type:law_nation_of_magic
	}
}