﻿COUNTRIES = {
	c:B49 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_theocracy
		activate_law = law_type:law_censorship
		activate_law = law_type:law_migration_controls

		activate_law = law_type:law_tenant_farmers #A main appeal of the Dominion for Ynnics is that they're not hardcore about serfdom like the Sarda
		activate_law = law_type:law_legacy_slavery #kobolds came to willingly become slaves in a bizarre turn of events when they heard of Varlen
		activate_law = law_type:law_professional_army #Drozmateers
		activate_law = law_type:law_interventionism #Is industrializing unlike most other ruinborn
		
		activate_law = law_type:law_same_heritage_only #aldanism
		activate_law = law_type:law_nation_of_artifice

		set_ruling_interest_groups = {
			ig_landowners ig_devout ig_armed_forces
		}
	}
}