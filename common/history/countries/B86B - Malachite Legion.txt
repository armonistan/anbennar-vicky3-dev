﻿COUNTRIES = {
	c:B86B ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = lathe
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_social_security
		
		
		activate_law = law_type:law_nation_of_artifice
	}
}