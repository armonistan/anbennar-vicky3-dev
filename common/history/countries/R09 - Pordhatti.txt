﻿COUNTRIES = {
	c:R09 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		activate_law = law_type:law_stratocracy
		activate_law = law_type:law_oligarchy #Until stratocracy is created
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned

		add_modifier = { name = hobgoblin_statocracy_modifier }
	}
}