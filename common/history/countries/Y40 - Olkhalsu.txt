﻿COUNTRIES = {
	c:Y40 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_censorship
		activate_law = law_type:law_appointed_bureaucrats

		add_technology_researched = urban_planning
	}
}