﻿COUNTRIES = {
	c:C04 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_expanded_tolerance
		
		activate_law = law_type:law_traditional_magic_banned
	}
}