﻿CHARACTERS = {
	c:B17 ?= {
		create_character = {
			template = ruler_elissa_template
			on_created = {
				set_character_immortal = yes
			}
		}
	}
}
