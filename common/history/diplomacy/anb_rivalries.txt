﻿DIPLOMACY = {
	c:A01 = {	#Anbennar-Lorent
		create_diplomatic_pact = {
			country = c:A03
			type = rivalry
		}
	}
	c:A03 = {	#Lorent-Northern League
		create_diplomatic_pact = {
			country = c:A04
			type = rivalry
		}
	}		
	c:A04 = {	#Northern League-Grombar
		create_diplomatic_pact = {
			country = c:A10
			type = rivalry
		}
	}	
	c:B29 = {	#Sarda-Dragon Dominion
		create_diplomatic_pact = {
			country = c:B49
			type = rivalry
		}
	}

	# c:B27 = { #Neratica-Trollsbay
	# 	create_diplomatic_pact = {
	# 		country = c:B98
	# 		type = rivalry
	# 	}
	# }

	c:B98 = { #Trollsbay-Lorent
		create_diplomatic_pact = {
			country = c:A03
			type = rivalry
		}
	}

	c:B41 = {	#Plumstead is still salty they were forced to release Beggaston
		create_diplomatic_pact = {
			country = c:B34
			type = rivalry
		}
	}

	c:B34 = {	#Beggaston is still salty about being occupied during the Decades of the Mountain
		create_diplomatic_pact = {
			country = c:B41
			type = rivalry
		}
	}

	c:R08 = { #Lion Command split from the main Command
		create_diplomatic_pact = {
			country = c:R17
			type = rivalry
		}
	}

	c:L01 = { #Konolkhatep-Busilar rivalry
		create_diplomatic_pact = {
			country = c:A09
			type = rivalry
		}
	}
	c:A09 = { #Busilar-Konolkhatep rivalry
		create_diplomatic_pact = {
			country = c:L01
			type = rivalry
		}
	}
}	




