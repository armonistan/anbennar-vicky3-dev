﻿ruinborn_kheionai = {
	template = "moon_elf"

	
	skin_color = {
		10 = { 0.76 0.7 0.80 0.78 }
	}
	eye_color = {
		# Brown
		50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		
		# Black
		95 = { 0.01 0.9 0.5 0.99 }
	}

	hairstyles = {
		10 = { name = european_hairstyles 		range = { 0.0 1.0 } }
	}
}