﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

ruinborn_kwineh = {
	template = "ruinborn_boek"

	#TODO these
    skin_color = {
		5 = { 0.76 0.9 0.8 0.95 }	
    }
	eye_color = {
		# Brown
		50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		
		# Black
		95 = { 0.01 0.9 0.5 0.99 }
	}

	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
		20 = { name = no_hairstyles 		range = { 0.0 1.0 } }
	}
	
    POD_NOSschnoz = {
        10 = { name = schnozies            range = { 0.7 1.0 } }	#extreme
    }

}

	