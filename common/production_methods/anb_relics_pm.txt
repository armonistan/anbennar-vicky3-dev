pm_relic_shovel = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_relics_add = 20
		}

		level_scaled = {
			building_employment_academics_add = 500
			building_employment_laborers_add = 4500
		}
	}
}

pm_relic_throwel_bucket = {
	texture = "gfx/interface/icons/production_method_icons/sweeteners.dds"
	
	unlocking_technologies = {
		empiricism
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			
			# output goods
			goods_output_relics_add = 35
		}

		level_scaled = {
			building_employment_academics_add = 1000
			building_employment_laborers_add = 4000
		}
	}
}

pm_relic_brushes_sieve = {
	texture = "gfx/interface/icons/production_method_icons/furniture_handicraft.dds"
	
	unlocking_technologies = {
		philosophical_pragmatism
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			
			# output goods
			goods_output_relics_add = 50
		}

		level_scaled = {
			building_employment_academics_add = 1500
			building_employment_laborers_add = 3500
		}
	}
}



pm_relics_no_repairs = {
	texture = "gfx/interface/icons/production_method_icons/unused/no_maintenance.dds"
}

pm_relics_manual_repairs = {
	texture = "gfx/interface/icons/production_method_icons/furniture_handicraft.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 5
			goods_input_damestear_add = 2
			
			# output goods
			goods_output_relics_add = 5
		}

		level_scaled = {
			building_employment_mages_add = 500
		}
	}
}

pm_relics_advanced_repairs = {
	texture = "gfx/interface/icons/production_method_icons/steel_tools.dds"
	
	unlocking_technologies = {
		punch_card_artificery
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 10
			goods_input_damestear_add = 2
			
			# output goods
			goods_output_relics_add = 8
		}

		level_scaled = {
			building_employment_mages_add = 500
		}
	}
}

pm_relics_advanceder_repairs = {
	texture = "gfx/interface/icons/production_method_icons/arc_welded_buildings.dds"
	
	unlocking_technologies = {
		magical_wiring
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 15
			goods_input_damestear_add = 2
			
			# output goods
			goods_output_relics_add = 14
		}

		level_scaled = {
			building_employment_mages_add = 400
			building_employment_machinists_add = 100
		}
	}
}

pm_relics_advancedest_repairs = {
	texture = "gfx/interface/icons/production_method_icons/electric_arc_process.dds"
	
	unlocking_technologies = {
		electroarcanism_theory
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 15
			goods_input_damestear_add = 2
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_relics_add = 22
		}

		level_scaled = {
			building_employment_mages_add = 250
			building_employment_machinists_add = 250
		}
	}
}

pm_relics_excavation_focus = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
}

pm_relics_dissassembly_focus = {
	texture = "gfx/interface/icons/production_method_icons/percussion_caps.dds"
	
	unlocking_production_methods = {
		pm_relic_throwel_bucket
		pm_relic_brushes_sieve
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			
			# output goods
			goods_output_relics_add = -30
			goods_output_flawless_metal_add = 10
			goods_output_damestear_add = 15
		}

		level_scaled = {
			building_employment_engineers_add = 250
			building_employment_machinists_add = 250
		}
	}
}

pm_relics_hand_picked = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_relics_automata_assistant = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 5
			}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_relics_automata_archeologist = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		advanced_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 11
			}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_academics_add = -1000
		}
	}
}

pm_relics_automata_assistant_enforced = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	

		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 6
			}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_relics_automata_archeologist_enforced = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		advanced_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 11
			}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_academics_add = -1000
		}
	}
}

pm_relics_magical_digging_tools = {
	texture = "gfx/interface/icons/production_method_icons/arc_welded_buildings.dds"
	
	unlocking_technologies = {
		fractional_distillation
	}
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_magical_reagents_add = 5
			}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}
}

#Archeostudies

pm_archeostudies_diploma = {
	texture = "gfx/interface/icons/production_method_icons/scholastic_education.dds"
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_max_add = 10
			country_prestige_add = 5
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_relics_add = 15
			goods_input_magical_reagents_add = 15
		}	
	
		level_scaled = {
			building_employment_clerks_add = 2000
			building_employment_laborers_add = 2000
			building_employment_academics_add = 3000
			building_employment_mages_add = 3000
		}
	}
	
	state_modifiers = {
		workforce_scaled = { 
			building_university_throughput_add = 0.25
		}	
	}
}

pm_archeostudies_degree = {
	texture = "gfx/interface/icons/production_method_icons/philosophy_dept.dds"
	
	unlocking_technologies = {
		dialectics
	}
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_max_add = 15
			country_prestige_add = 10
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_relics_add = 30
			goods_input_magical_reagents_add = 30
		}	
	
		level_scaled = {
			building_employment_clerks_add = 2250
			building_employment_laborers_add = 1750
			building_employment_academics_add = 3250
			building_employment_mages_add = 2750
		}
	}
	
	state_modifiers = {
		workforce_scaled = { 
			building_university_throughput_add = 0.50
		}	
	}
}

pm_archeostudies_masters = {
	texture = "gfx/interface/icons/production_method_icons/analytical_philosophy_department.dds"
	
	unlocking_technologies = {
		analytical_philosophy
	}
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_max_add = 20
			country_prestige_add = 20
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_relics_add = 50
			goods_input_magical_reagents_add = 40
			goods_input_artificery_doodads_add = 10
		}	
	
		level_scaled = {
			building_employment_clerks_add = 2500
			building_employment_laborers_add = 1500
			building_employment_academics_add = 3500
			building_employment_mages_add = 2000
			building_employment_engineers_add = 500
		}
	}
	
	state_modifiers = {
		workforce_scaled = { 
			building_university_throughput_add = 0.75
		}	
	}
}

pm_archeostudies_phd = {
	texture = "gfx/interface/icons/production_method_icons/analytical_philosophy_department.dds"
	
	unlocking_technologies = {
		psychoanalysis
	}
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_max_add = 25
			country_prestige_add = 40
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_relics_add = 90
			goods_input_magical_reagents_add = 30
			goods_input_artificery_doodads_add = 30
		}	
	
		level_scaled = {
			building_employment_clerks_add = 2750
			building_employment_laborers_add = 1250
			building_employment_academics_add = 3750
			building_employment_mages_add = 1250
			building_employment_engineers_add = 1000
		}
	}
	
	state_modifiers = {
		workforce_scaled = { 
			building_university_throughput_add = 1.0
		}	
	}
}	